//
//  ConversationNewTableViewCell.swift
//  Firebase Chat App
//
//  Created by Rahul Dhiman on 29/12/21.
//

import UIKit
import SDWebImage

class ConversationNewTableViewCell: UITableViewCell {
    
    static let identifier = "ConversationNewTableViewCell"

    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userChatNameLabel: UILabel!
    @IBOutlet weak var userChatMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupView()
    }
    
    private func setupView() {
        userProfileImageView.layer.cornerRadius = 5
        userProfileImageView.clipsToBounds = true
        userProfileImageView.contentMode = .scaleAspectFill
        
    }
    
    public func configCell(model: Conversation) {
        userChatNameLabel.text = model.name
        userChatMessageLabel.text = model.lastedMessage.text
        
        let path = "images/\(model.otherUserEmail)_profile_picture.png"
        storageManager.shared.downloadURL(for: path, completion: { [weak self] result in
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userProfileImageView.sd_setImage(with: url, completed: nil)
                }
            case .failure(let error):
                print("failed to get image \(error)")
            }
        })
    }
    
}
